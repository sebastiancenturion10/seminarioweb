let dataPaciente = {};
let signosVitales = [];

// Traer el codigo del paciente elegido para pedir datos;
const getCodigoPaciente = () => localStorage.getItem('codigo');

const getPacienteData = async () => {
    const codigo = getCodigoPaciente();
    /*
     * Tipo de respuesta success
     * {data: apellido: string, codigo: number, fecha_nacimiento: date, nombre: string, success: boolean}
     */
    try {
        let res = await fetch(`http://localhost:8080/api/pacientes/${codigo}`, {
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json();
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

const updatePacienteData = async ({ nombre, apellido, fecha_nacimiento }) => {
    const codigo = getCodigoPaciente();
    try {
        let res = await fetch(`http://localhost:8080/api/pacientes/${codigo}`, {
            method: 'PUT',
            body: JSON.stringify({ nombre, apellido, fecha_nacimiento }),
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json();
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

getPacienteData().then((data) => {
    dataPaciente = data;
    const nombreApellido = document.getElementById("datos-paciente"); // Referencia elemento HTML;
    nombreApellido.innerHTML = `${dataPaciente.data.nombre} ${dataPaciente.data.apellido}`; // Agregar datos del paciente;
    const fechaNacimiento = document.getElementById("fecha-nacimiento"); // Referencia elemento HTML;

    // Formatear fecha para el usuario y para el HTML requiere otro formato
    let fecha_nacimiento_ES = new Date(dataPaciente.data.fecha_nacimiento).toLocaleDateString();
    let fecha_nacimiento_US = new Date(dataPaciente.data.fecha_nacimiento);
    fechaNacimiento.innerHTML = fecha_nacimiento_ES; // Agregar datos del paciente;

    // Setear los inputs
    document.getElementById('nombre').value = dataPaciente.data.nombre;
    document.getElementById('apellido').value = dataPaciente.data.apellido;
    // pasar a formato ISO 2022-12-08T03:00:00.000Z y separar desde la T para tener formato YYYY-mm-dd (index 0 date formateado)
    document.getElementById('fecha_nacimiento').value = fecha_nacimiento_US.toISOString().split('T')[0]
})

const showInputs = () => {
    // MOSTRAR U OCULTAR ELEMENTOS DE ACUERDO A CLICKS EN BOTONES
    let infoPaciente = document.getElementById("info-paciente");
    let formPaciente = document.getElementById("form-paciente");
    if (infoPaciente.style.display === "none") {
        infoPaciente.style.display = "block";
        formPaciente.style.display = "none";
    } else {
        infoPaciente.style.display = "none";
        formPaciente.style.display = "block";
    }
}

const actulizarPaciente = () => {
    if (document.getElementById('nombre').value &&
        document.getElementById('apellido').value &&
        document.getElementById('fecha_nacimiento').value) {
        const body = {
            nombre: document.getElementById('nombre').value,
            apellido: document.getElementById('apellido').value,
            fecha_nacimiento: document.getElementById('fecha_nacimiento').value
        }
        updatePacienteData({ ...body }).then((response) => {
            window.location.reload();
        })
    } else {
        let showError = document.getElementById("show-creacion-error");
        if (showError.style.display === "none") {
            showError.style.display = "block";
        }
    }
}


// SIGNOS VITALES
const getSignosVitales = async () => {
    const codigoPaciente = getCodigoPaciente();
    try {
        let res = await fetch(`http://localhost:8080/api/pacientes/signosVitales/${codigoPaciente}`, {
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json();
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

getSignosVitales().then(res => {
    // Capturar error http
    if (res.message) {
        const noData = `<div role="alert" class="rounded border-l-4 border-red-500 bg-red-50 p-4">
        <strong class="block font-medium text-red-700"> No se encontraron signos vitales cargados. </strong>
        <p class="mt-2 text-sm text-red-700">
            Puede cargar los signos vitales para el paciente utilizando el botón correspondiente.
        </p>
        </div>`
        const signosVitalesSection = document.getElementById("signosVitalesSection");
        signosVitalesSection.innerHTML = noData;
        return;
    }
    if (res.data) {
        signosVitales = res.data
        const signosData = signosVitales
            // mapear a HTML para mostrar en la interfaz
            .map((signoVital) => {
                // Formatear fecha para el usuario
                let fecha = new Date(signoVital.fecha).toLocaleDateString();
                return `<div class="mt-4 text-gray-500 sm:pr-8 border border-gray p-5">
                    <h3 class="mt-4 text-xl font-bold text-gray-900">${fecha}</h3>
                    <p class="mt-2 hidden text-md sm:block flex flex-col">
                        <b> Frecuencia: </b> ${signoVital.frecuencia}
                        <br>
                        <b> Presión: </b>${signoVital.presion}
                    </p>
                    <div class="flex space-x-3 items-center mt-2">
                        <img src="delete.png" width="32" alt="Delete" class="mt-2 cursor-pointer" onclick="onSignosDelete(${signoVital.id})">
                        <button type="button" onclick="editarSignoVitales({...signosVitales})"
                            class="rounded-lg bg-blue-500 px-5 py-3 text-sm font-medium text-white">
                            Editar
                        </button>
                    </div>
                </div>`;
            })
            .join("");

        const signosVitalesSection = document.getElementById("signosVitalesSection");
        signosVitalesSection.innerHTML = signosData;
    }
})

const postSignosVitalesData = async ({ fecha, frecuencia, presion, codigo_paciente }) => {
    try {
        let res = await fetch(`http://localhost:8080/api/pacientes/signosVitales`, {
            method: 'POST',
            body: JSON.stringify({ fecha, frecuencia, presion, codigo_paciente }),
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json();
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

const createSignosVitales = () => {
    if (document.getElementById('fecha').value &&
        document.getElementById('fecha').value &&
        document.getElementById('fecha').value) {
        const body = {
            fecha: document.getElementById('fecha').value,
            frecuencia: document.getElementById('frecuencia').value,
            presion: document.getElementById('presion').value,
            codigo_paciente: getCodigoPaciente()
        }
        postSignosVitalesData({ ...body }).then(() => {
            window.location.reload();
        })
    } else {
        let showError = document.getElementById("show-error");
        if (showError.style.display === "none") {
            showError.style.display = "block";
        }
    }

}

const showCreateSignos = () => {
    // MOSTRAR U OCULTAR ELEMENTOS DE ACUERDO A CLICKS EN BOTONES
    let form = document.getElementById("form-signos-vitales");
    if (form.style.display === "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}

const onSignosDelete = async (id) => {
    let opcion = confirm("Está seguro que desea borrar los signos vitales? Esta acción es irreversible");
    if (opcion == true) {
        try {
            let res = await fetch(`http://localhost:8080/api/pacientes/signosVitales/${id}`, {
                method: 'DELETE',
                headers: {
                    "Content-Type": "application/json",
                },
            });
            let data = res.json();
            window.location.href = "detalle-paciente.html";
            return data;
        } catch (error) {
            console.log(
                "Hubo un error al fetchear los datos. Revisar conexión a BD",
                error
            );
            return;
        }
    } else {

    }
};

const updateSignosVitalesData = async ({ fecha, frecuencia, presion }) => {
    const id = signosVitales[0].id;
    try {
        let res = await fetch(`http://localhost:8080/api/pacientes/signosVitales/${id}`, {
            method: 'PUT',
            body: JSON.stringify({ fecha, frecuencia, presion }),
            headers: {
                "Content-Type": "application/json",
            },
        });
        let data = res.json();
        return data;
    } catch (error) {
        console.log(
            "Hubo un error al fetchear los datos. Revisar conexión a BD",
            error
        );
        return;
    }
};

const actualizarSignosVitales = () => {
    const body = {
        fecha: document.getElementById('fecha_edicion').value,
        frecuencia: document.getElementById('frecuencia_edicion').value,
        presion: document.getElementById('presion_edicion').value
    }
    updateSignosVitalesData({ ...body }).then((response) => {
        window.location.reload();
    })
}

const cancelarEdicion = () => {
    let form = document.getElementById("editar-signos-vitales");
    if (form.style.display === "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}

const editarSignoVitales = (signoVital) => {
    // Setear los inputs para editar
    let fecha_US = new Date(signoVital[0].fecha);
    document.getElementById('frecuencia_edicion').value = signoVital[0].frecuencia;
    document.getElementById('presion_edicion').value = signoVital[0].presion;
    document.getElementById('fecha_edicion').value = fecha_US.toISOString().split('T')[0]
    let form = document.getElementById("editar-signos-vitales");
    if (form.style.display === "none") {
        form.style.display = "block";
    } else {
        form.style.display = "none";
    }
}